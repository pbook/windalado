﻿using DalAdoNet.Entities;
using System;
using System.Windows.Forms;

namespace PhonebookAdoNet
{
    public partial class FormAddEditContact : Form
    {
        private Contact contact;
        public FormAddEditContact(Contact contact)
        {
            InitializeComponent();
            this.contact = contact;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxName.Text))
            {
                contact.Name = textBoxName.Text;
            }
            else
            {
                MessageBox.Show("Name must have a value.");
                return;
            }

            if (!string.IsNullOrEmpty(textBoxEmail.Text))
            {
                contact.Email = textBoxEmail.Text;
            }
            else
            {
                MessageBox.Show("Email must have a value.");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }

        private void FormAddEditContact_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("{0} Contact - Phonebook", contact.Id > 0 ? "Edit" : "Add");
            this.textBoxName.Text = contact.Name;
            this.textBoxEmail.Text = contact.Email;
        }
    }
}
