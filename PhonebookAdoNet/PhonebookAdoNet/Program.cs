﻿using DalAdoNet.Repositories;
using System;
using System.Windows.Forms;

namespace PhonebookAdoNet
{
    static class Program
    {
        private static readonly string connectionString = @"Server=.\SQLEXPRESS;Initial Catalog=PhonebookAdoNet; Integrated Security = true;";
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            ContactRepository contactRepository = new ContactRepository(connectionString);
            PhoneRepository phoneRepository = new PhoneRepository(connectionString);
            Application.Run(new FormMain(contactRepository, phoneRepository));
        }
    }
}
