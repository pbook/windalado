﻿using DalAdoNet.Entities;
using DalAdoNet.Repositories;
using System;
using System.Windows.Forms;

namespace PhonebookAdoNet
{
    public partial class FormMain : Form
    {
        private ContactRepository contactRepository;
        private PhoneRepository phoneRepository;

        public FormMain(ContactRepository contactRepository, PhoneRepository phoneRepository)
        {
            InitializeComponent();
            this.contactRepository = contactRepository;
            this.phoneRepository = phoneRepository;

            RefreshContacts();
        }

        private void RefreshContacts()
        {
            bindingSourceContacts.DataSource = contactRepository.GetAll();
            dataGridViewContacts.DataSource = bindingSourceContacts;
        }

        private void RefreshPhones()
        {
            Contact contact = (Contact)bindingSourceContacts.Current;
            if (contact == null)
            {
                return;
            }

            bindingSourcePhones.DataSource = phoneRepository.GetAll(contact.Id);
            dataGridViewPhones.DataSource = bindingSourcePhones;
        }

        private void bindingSourceContacts_CurrentChanged(object sender, EventArgs e)
        {
            RefreshPhones();
        }

        private void toolStripButtonAddContact_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();
            FormAddEditContact form = new FormAddEditContact(contact);

            if (form.ShowDialog() == DialogResult.OK)
            {
                contactRepository.Insert(contact);
                RefreshContacts();
            }
        }

        private void toolStripButtonEditContact_Click(object sender, EventArgs e)
        {
            Contact contact = (Contact)bindingSourceContacts.Current;
            if (contact == null)
            {
                return;
            }

            FormAddEditContact form = new FormAddEditContact(contact);

            if (form.ShowDialog() == DialogResult.OK)
            {
                contactRepository.Update(contact);
                RefreshContacts();
            }
        }

        private void toolStripButtonDeleteContact_Click(object sender, EventArgs e)
        {
            Contact contact = (Contact)bindingSourceContacts.Current;
            if (contact == null)
            {
                return;
            }

            DialogResult result = MessageBox.Show(
                "Are you sure you want to delete this contact?",
                "Confirmation",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2);

            if (result == DialogResult.Yes)
            {
                contactRepository.Delete(contact);
                RefreshContacts();
            }
        }

        private void toolStripButtonAddPhone_Click(object sender, EventArgs e)
        {
            Contact contact = (Contact)bindingSourceContacts.Current;
            if (contact == null)
            {
                return;
            }

            Phone phone = new Phone();
            phone.ContactId = contact.Id;

            FormAddEditPhone form = new FormAddEditPhone(phone);
            if (form.ShowDialog() == DialogResult.OK)
            {
                phoneRepository.Insert(phone);
                RefreshPhones();
            }
        }

        private void toolStripButtonEditPhone_Click(object sender, EventArgs e)
        {
            Phone phone = (Phone)bindingSourcePhones.Current;
            if (phone == null)
            {
                return;
            }

            FormAddEditPhone form = new FormAddEditPhone(phone);
            if (form.ShowDialog() == DialogResult.OK)
            {
                phoneRepository.Update(phone);
                RefreshPhones();
            }
        }

        private void toolStripButtonDeletePhone_Click(object sender, EventArgs e)
        {
            Phone phone = (Phone)bindingSourcePhones.Current;
            if (phone == null)
            {
                return;
            }

            DialogResult result = MessageBox.Show(
                "Are you sure you want to delete this phone?",
                "Confirmation",
                MessageBoxButtons.YesNo,
                MessageBoxIcon.Question,
                MessageBoxDefaultButton.Button2
                );

            if (result == DialogResult.Yes)
            {
                phoneRepository.Delete(phone);
                RefreshPhones();
            }
        }
    }
}
