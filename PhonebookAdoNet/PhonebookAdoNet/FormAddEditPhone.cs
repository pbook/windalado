﻿using DalAdoNet.Entities;
using System;
using System.Windows.Forms;

namespace PhonebookAdoNet
{
    public partial class FormAddEditPhone : Form
    {
        private Phone phone;
        public FormAddEditPhone(Phone phone)
        {
            InitializeComponent();

            this.phone = phone;
        }

        private void FormAddEditPhone_Load(object sender, EventArgs e)
        {
            this.Text = string.Format("{0} Phone - Phonebook", phone.Id > 0 ? "Edit" : "Add");

            this.textBoxNumber.Text = phone.Number;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBoxNumber.Text))
            {
                phone.Number = textBoxNumber.Text;
            }
            else
            {
                MessageBox.Show("Number must have a value.");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }
    }
}
