﻿namespace PhonebookAdoNet
{
    partial class FormMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
            this.toolStripContacts = new System.Windows.Forms.ToolStrip();
            this.toolStripPhones = new System.Windows.Forms.ToolStrip();
            this.dataGridViewContacts = new System.Windows.Forms.DataGridView();
            this.dataGridViewPhones = new System.Windows.Forms.DataGridView();
            this.toolStripButtonAddContact = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEditContact = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDeleteContact = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonEditPhone = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonDeletePhone = new System.Windows.Forms.ToolStripButton();
            this.bindingSourceContacts = new System.Windows.Forms.BindingSource(this.components);
            this.bindingSourcePhones = new System.Windows.Forms.BindingSource(this.components);
            this.toolStripContacts.SuspendLayout();
            this.toolStripPhones.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceContacts)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePhones)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripContacts
            // 
            this.toolStripContacts.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonAddContact,
            this.toolStripButtonEditContact,
            this.toolStripButtonDeleteContact});
            this.toolStripContacts.Location = new System.Drawing.Point(0, 0);
            this.toolStripContacts.Name = "toolStripContacts";
            this.toolStripContacts.Size = new System.Drawing.Size(407, 25);
            this.toolStripContacts.TabIndex = 0;
            this.toolStripContacts.Text = "toolStrip1";
            // 
            // toolStripPhones
            // 
            this.toolStripPhones.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton4,
            this.toolStripButtonEditPhone,
            this.toolStripButtonDeletePhone});
            this.toolStripPhones.Location = new System.Drawing.Point(0, 175);
            this.toolStripPhones.Name = "toolStripPhones";
            this.toolStripPhones.Size = new System.Drawing.Size(407, 25);
            this.toolStripPhones.TabIndex = 1;
            this.toolStripPhones.Text = "toolStrip2";
            // 
            // dataGridViewContacts
            // 
            this.dataGridViewContacts.AllowUserToAddRows = false;
            this.dataGridViewContacts.AllowUserToDeleteRows = false;
            this.dataGridViewContacts.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewContacts.Dock = System.Windows.Forms.DockStyle.Top;
            this.dataGridViewContacts.Location = new System.Drawing.Point(0, 25);
            this.dataGridViewContacts.Name = "dataGridViewContacts";
            this.dataGridViewContacts.ReadOnly = true;
            this.dataGridViewContacts.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewContacts.Size = new System.Drawing.Size(407, 150);
            this.dataGridViewContacts.TabIndex = 2;
            // 
            // dataGridViewPhones
            // 
            this.dataGridViewPhones.AllowUserToAddRows = false;
            this.dataGridViewPhones.AllowUserToDeleteRows = false;
            this.dataGridViewPhones.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewPhones.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewPhones.Location = new System.Drawing.Point(0, 200);
            this.dataGridViewPhones.Name = "dataGridViewPhones";
            this.dataGridViewPhones.ReadOnly = true;
            this.dataGridViewPhones.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dataGridViewPhones.Size = new System.Drawing.Size(407, 106);
            this.dataGridViewPhones.TabIndex = 3;
            // 
            // toolStripButtonAddContact
            // 
            this.toolStripButtonAddContact.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonAddContact.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonAddContact.Image")));
            this.toolStripButtonAddContact.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonAddContact.Name = "toolStripButtonAddContact";
            this.toolStripButtonAddContact.Size = new System.Drawing.Size(33, 22);
            this.toolStripButtonAddContact.Text = "Add";
            this.toolStripButtonAddContact.Click += new System.EventHandler(this.toolStripButtonAddContact_Click);
            // 
            // toolStripButtonEditContact
            // 
            this.toolStripButtonEditContact.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonEditContact.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonEditContact.Image")));
            this.toolStripButtonEditContact.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEditContact.Name = "toolStripButtonEditContact";
            this.toolStripButtonEditContact.Size = new System.Drawing.Size(31, 22);
            this.toolStripButtonEditContact.Text = "Edit";
            this.toolStripButtonEditContact.Click += new System.EventHandler(this.toolStripButtonEditContact_Click);
            // 
            // toolStripButtonDeleteContact
            // 
            this.toolStripButtonDeleteContact.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeleteContact.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeleteContact.Image")));
            this.toolStripButtonDeleteContact.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeleteContact.Name = "toolStripButtonDeleteContact";
            this.toolStripButtonDeleteContact.Size = new System.Drawing.Size(44, 22);
            this.toolStripButtonDeleteContact.Text = "Delete";
            this.toolStripButtonDeleteContact.Click += new System.EventHandler(this.toolStripButtonDeleteContact_Click);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(33, 22);
            this.toolStripButton4.Text = "Add";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButtonAddPhone_Click);
            // 
            // toolStripButtonEditPhone
            // 
            this.toolStripButtonEditPhone.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonEditPhone.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonEditPhone.Image")));
            this.toolStripButtonEditPhone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonEditPhone.Name = "toolStripButtonEditPhone";
            this.toolStripButtonEditPhone.Size = new System.Drawing.Size(31, 22);
            this.toolStripButtonEditPhone.Text = "Edit";
            this.toolStripButtonEditPhone.Click += new System.EventHandler(this.toolStripButtonEditPhone_Click);
            // 
            // toolStripButtonDeletePhone
            // 
            this.toolStripButtonDeletePhone.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonDeletePhone.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonDeletePhone.Image")));
            this.toolStripButtonDeletePhone.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonDeletePhone.Name = "toolStripButtonDeletePhone";
            this.toolStripButtonDeletePhone.Size = new System.Drawing.Size(44, 22);
            this.toolStripButtonDeletePhone.Text = "Delete";
            this.toolStripButtonDeletePhone.Click += new System.EventHandler(this.toolStripButtonDeletePhone_Click);
            // 
            // bindingSourceContacts
            // 
            this.bindingSourceContacts.CurrentChanged += new System.EventHandler(this.bindingSourceContacts_CurrentChanged);
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(407, 306);
            this.Controls.Add(this.dataGridViewPhones);
            this.Controls.Add(this.toolStripPhones);
            this.Controls.Add(this.dataGridViewContacts);
            this.Controls.Add(this.toolStripContacts);
            this.Name = "FormMain";
            this.Text = "FormMain";
            this.toolStripContacts.ResumeLayout(false);
            this.toolStripContacts.PerformLayout();
            this.toolStripPhones.ResumeLayout(false);
            this.toolStripPhones.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewPhones)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourceContacts)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bindingSourcePhones)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStripContacts;
        private System.Windows.Forms.ToolStrip toolStripPhones;
        private System.Windows.Forms.DataGridView dataGridViewContacts;
        private System.Windows.Forms.DataGridView dataGridViewPhones;
        private System.Windows.Forms.BindingSource bindingSourceContacts;
        private System.Windows.Forms.BindingSource bindingSourcePhones;
        private System.Windows.Forms.ToolStripButton toolStripButtonAddContact;
        private System.Windows.Forms.ToolStripButton toolStripButtonEditContact;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeleteContact;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripButton toolStripButtonEditPhone;
        private System.Windows.Forms.ToolStripButton toolStripButtonDeletePhone;
    }
}