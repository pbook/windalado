CREATE DATABASE [PhonebookAdoNet]

GO
USE [PhonebookAdoNet]

CREATE TABLE [Contacts]
(
	[Id] int IDENTITY(1,1) PRIMARY KEY,
	[Name] NVARCHAR(50) NOT NULL,
	[Email] VARCHAR(50) NOT NULL
)

CREATE TABLE [Phones]
(
	[Id] int IDENTITY(1,1) PRIMARY KEY,
	[ContactId] int NOT NULL FOREIGN KEY REFERENCES [Contacts] ([Id]) ON DELETE CASCADE,
	[Number] VARCHAR(20) NOT NULL	
)

GO
INSERT INTO Contacts ([Name], Email)
VALUES ('John Smith', 'john@smith.com')

INSERT INTO Contacts ([Name], Email)
VALUES ('Marry Smith', 'marry@smith.com')

INSERT INTO Phones (ContactId, Number)
VALUES (1, '0888 989898')

INSERT INTO Phones (ContactId, Number)
VALUES (1, '0878 787878')

INSERT INTO Phones (ContactId, Number)
VALUES (2, '0898 717273')