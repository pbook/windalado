﻿using DalAdoNet.Entities;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace DalAdoNet.Repositories
{
    public class PhoneRepository
    {
        private readonly string connectionString;

        public PhoneRepository(string connectionString)
        {
            this.connectionString = connectionString;
        }

        public List<Phone> GetAll(int contactId)
        {
            List<Phone> resultSet = new List<Phone>();
            IDbConnection connection = new SqlConnection(connectionString);

            try
            {
                connection.Open();
                IDbCommand command = connection.CreateCommand();
                command.CommandText =
@"
SELECT * FROM Phones 
WHERE ContactId = @ContactId
";

                IDataParameter parameter = command.CreateParameter();
                parameter.ParameterName = "@ContactId";
                parameter.Value = contactId;
                command.Parameters.Add(parameter);

                IDataReader reader = command.ExecuteReader();
                using (reader)
                {
                    while (reader.Read())
                    {
                        resultSet.Add(new Phone()
                        {
                            Id = (int)reader["Id"],
                            ContactId = (int)reader["ContactId"],
                            Number = (string)reader["Number"]
                        });
                    }
                }
            }
            finally
            {
                connection.Close();
            }

            return resultSet;
        }

        public void Insert(Phone phone)
        {
            IDbConnection connection = new SqlConnection(connectionString);

            IDbCommand command = connection.CreateCommand();
            command.CommandText =
@"
INSERT INTO Phones (ContactId, Number) 
VALUES (@ContactId, @Number)
";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@ContactId";
            parameter.Value = phone.ContactId;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Number";
            parameter.Value = phone.Number;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }

        public void Update(Phone Phone)
        {
            IDbConnection connection = new SqlConnection(connectionString);

            IDbCommand command = connection.CreateCommand();
            command.CommandText =
@"
UPDATE Phones 
SET 
    Number=@Number 
WHERE Id=@Id
";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = Phone.Id;
            command.Parameters.Add(parameter);

            parameter = command.CreateParameter();
            parameter.ParameterName = "@Number";
            parameter.Value = Phone.Number;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }


        public void Delete(Phone Phone)
        {
            IDbConnection connection = new SqlConnection(connectionString);


            IDbCommand command = connection.CreateCommand();
            command.CommandText =
@"
DELETE FROM Phones 
WHERE Id=@Id
";

            IDataParameter parameter = command.CreateParameter();
            parameter.ParameterName = "@Id";
            parameter.Value = Phone.Id;
            command.Parameters.Add(parameter);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
