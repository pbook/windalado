﻿namespace DalAdoNet.Entities
{
    public class Phone
    {
        public int Id { get; set; }
        public int ContactId { get; set; }
        public string Number { get; set; }
    }
}
